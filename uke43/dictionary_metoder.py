# Opprette en dictionary
d = {} # Mac SHIFT+ALT+ 8 9

print('Skriv inn key og verdi. Avslutt med tom streng.')

key = input('Key: ')

while(key!=''):
    value = input('Value: ')
    d[key] = value

    key = input('Key: ')

# Litt variasjon på bruk av f strings og ikke, for å minne dere på...
print(f'len(d): {len(d)}')
print('d.items():',d.items())
print('d.keys():',d.keys())
print('d.values():',d.values())
print('d.get(\'ost\',-999):',d.get('ost',-999))
print('Merk at alt er strenger. Hvordan kan vi lagre det som andre typer variable?')

# uskrift av alt:
for key, value in d.items():
    print(f'Inni skuffen {key} ligger {value}.')